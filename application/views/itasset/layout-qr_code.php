<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>QR Code</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		  <!-- Bootstrap 3.3.6 -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css">
		  <!-- Font Awesome -->
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		  <!-- Ionicons -->
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		  <!-- Theme style -->
	      <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/AdminLTE.min.css">
	       <!-- Custom CSS -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/style.css">
		  <!-- AdminLTE Skins. Choose a skin from the css/skins. -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/skins/skin-blue.min.css">
		  <!-- jQuery 2.2.3 -->
		  <script src="<?= base_url() ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
		  <!-- jQuery UI 1.11.4 -->
		  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
          <style type="text/css" media="print">
            @page {
                size: auto;   /* auto is the initial value */
                margin: 0;  /* this affects the margin in the printer settings */
            }
            </style>

</head>
<body>
            
           <div class="container" style="text-align:center;" >
            <div class="col-md-1">
            </div>
            <div id = 'printarea'>
                    <div class="col-md-10" style="text-align:left">
                            <h1>
                            <?php foreach($item_detail as $row): ?>
                                <?= $row['asset_name']; ?>
                                <br>
                            </h1>

                            <br>
                            
                            <div id="print_qrcode">
                                <div class="row">
                                        <div class="col-md-6" style="text-align:left">
                                            <div class="img">
                                                    <!-- <br><br> -->
                                                <img src="<?php echo base_url('assets/images/qrcode/')?><?php foreach($item_detail as $row): ?><?= trim($row['qr_code_img']); ?><?php endforeach; ?>" alt="" 
                                                style="width:98%;text-align:left;"
                                                > 
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <P style="text-align:right;">
                                                <?php foreach($item_detail as $row): ?>
                                                <?= trim($row['qr_code_id']); ?>
                                                <?php endforeach; ?>
                                            </P>
                                        </div>
                                </div>                                
                            </div>

                            <!-- <h1>
                            <?php endforeach; ?>
                               
                                
                            </h1> -->
                            <br>
                        
                    </div>
            </div> <!-- end of print area -->
            <div class="col-md-1">
            
            </div>
           </div>
           <br>
            <div class="container">
                <div class="col-md-12" style="text-align:center;">
                <input type='button'  class="btn btn-primary" id='btn' value='Print Barcode' onclick='printFunc();' style="width:50%;text-align:center;">
                </div>
            </div>
                
<script>
    function printFunc() {
    // var divToPrint = document.getElementById('printarea');
    var divToPrint = document.getElementById('print_qrcode');
    var htmlToPrint = ''; 
    // '' +
    //     '<style type="text/css">' +
    //     'table th, table td {' +
    //     'border:1px solid #000;' +
    //     'padding;0.5em;' +
    //     '}' +
    //     '</style>';
    htmlToPrint += divToPrint.outerHTML;
    newWin = window.open("");
    // newWin.document.write("<h3 align='center'>Print QR Code</h3>");
    // newWin.document.write(htmlToPrint);
     newWin.document.write(htmlToPrint)
    newWin.print();
    newWin.close();
    }
</script>

</body>
</html>
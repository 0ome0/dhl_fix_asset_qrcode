<!DOCTYPE html>
<html lang="en">
	<head>
		  <title><?=isset($title)?$title:'Inventory Management System' ?></title>
		  <!-- Tell the browser to be responsive to screen width -->
		  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		  <!-- Bootstrap 3.3.6 -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css">
		  <!-- Font Awesome -->
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		  <!-- Ionicons -->
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		  <!-- Theme style -->
	      <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/AdminLTE.min.css">
	       <!-- Custom CSS -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/style.css">
		  <!-- AdminLTE Skins. Choose a skin from the css/skins. -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/skins/skin-blue.min.css">
		  <!-- jQuery 2.2.3 -->
		  <script src="<?= base_url() ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
		  <!-- jQuery UI 1.11.4 -->
		  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

		
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper" style="height: auto; ">
			 <?php if($this->session->flashdata('msg') != ''): ?>
			    <div class="alert alert-warning flash-msg alert-dismissible">
			      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			      <h4> Success!</h4>
			      <?= $this->session->flashdata('msg'); ?> 
			    </div>
			  <?php endif; ?> 
			
			<section id="container">
				<!--header start-->
				<header class="header white-bg">
                    <?php include('include-page/navbar-asset.php'); ?>
				</header>
				<!--header end-->
				<!--sidebar start-->
				<aside>

          <?php
          $user_role = ($this->session->userdata('role'));

            // if (($this->session->userdata('role')) == '1') {
            //   include('include/sidebar-inven.php');
            // } else 
            //   include('include/sidebar-inven-user.php');
            include('include-page/sidebar-asset.php');
          ?>

				</aside>
				<!--sidebar end-->
				<!--main content start-->
				<section id="main-content">
					<div class="content-wrapper" style="min-height: 394px; padding:15px; background-image: url('<?= base_url('assets/images/whse.jpg'); ?>') ; no-repeat; background-size:cover;">
						<!-- page start-->
						 <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
      <h1>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Admin</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section> -->

    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-md-12" style="color : white;" >
        <h1 style = ' padding : 1%;'>Dashboard Overview</h1>
        <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>
              
              <?php foreach($unassign_owner as $row): ?>
              <?= $row['number_unassign_item']; ?> Unassign Owner
              <?php endforeach; ?>
              </h3>

              <p>Unassign Owner Item</p>
            </div>
            <div class="icon">
              <i class="ion ion-alert"></i>
            </div>
            <a href="<?= base_url('itasset/item_asset/unassign_owner'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
       <!-- ./col -->

       
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3> 
                  <?php foreach($assign_owner as $row): ?>
                  <?= $row['number_assign_item']; ?> Assigned Owner
                  <?php endforeach; ?>
              </h3>

              <p>IT Asset Assigned Owner</p>
            </div>
            <div class="icon">
              <i class="ion ion-clipboard"></i>
            </div>
            <a href="<?= base_url('itasset/item_asset/assigned_owner');  ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
                <?php foreach($asset_item as $row): ?>
                <?= $row['number_itasset']; ?> Asset Item
                <?php endforeach; ?>
              </h3>

              <p>Number of Asset Item</p>
            </div>
            <div class="icon">
              <i class="ion ion-cube"></i>
            </div>
            <a href="<?= base_url('itasset/item_asset/item_asset_management'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>

      <!-- row2 -->
      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3>
                <?php foreach($asset_expired as $row): ?>
                <?= $row['number_asset_expired']; ?> Asset Expired
                <?php endforeach; ?>
              </h3>

              <p> Asset Expired</p>
            </div>
            <div class="icon">
                <i class="ion-android-settings"></i>
            </div>
            <a href="<?= base_url('itasset/item_asset/asset_expired'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
       <!-- ./col -->

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>
                <?php foreach($vendor as $row): ?>
                <?= $row['number_vendor']; ?> Vendor
                <?php endforeach; ?>
              </h3>
              </h3>

              <p>Vendor</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-contacts"></i>
            </div>
            <a href="<?= base_url('itasset/vendor/vendor_management'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>
                  <?php foreach($asset_owner as $row): ?>
                  <?= $row['number_asset_owner']; ?> Asset Owner
                  <?php endforeach; ?>
              </h3>

              <p>Asset Owner </p>
            </div>
            <div class="icon">
              <i class="ion ion-android-person"></i>
            </div>
            <a href="<?= base_url('itasset/asset_owner/asset_owner_management'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>
                <?php foreach($asset_expired_1 as $row): ?>
                <?= $row['number_asset_expired']; ?> Expire in 0-30 days
                <?php endforeach; ?>
              </h3>

              <p> Asset Expire in 0-30 days</p>
            </div>
            <div class="icon">
                <i class="ion ion-alert"></i>
            </div>
            <a href="<?= base_url('itasset/item_asset/asset_expired_in_days/1'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>
                <?php foreach($asset_expired_2 as $row): ?>
                <?= $row['number_asset_expired']; ?> Expire in 31-60 days
                <?php endforeach; ?>
              </h3>

              <p> Asset Expire in 31-60 days</p>
            </div>
            <div class="icon">
                <i class="ion ion-alert"></i>
            </div>
            <a href="<?= base_url('itasset/item_asset/asset_expired_in_days/2'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
                <?php foreach($asset_expired_3 as $row): ?>
                <?= $row['number_asset_expired']; ?> Expire in 61-90 days
                <?php endforeach; ?>
              </h3>

              <p> Asset Expire in 61-90 days</p>
            </div>
            <div class="icon">
                <i class="ion ion-alert"></i>
            </div>
            <a href="<?= base_url('itasset/item_asset/asset_expired_in_days/3'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col -->
      </div>
    


      
    </section>
    <!-- /.content -->


<script>
$("#examples").addClass('active');
$("#blank-page").addClass('active');
</script>
						<!-- page end-->
					</div>
				</section>
				<!--main content end-->
				<!--footer start-->
				<footer class="main-footer">
					<strong>Copyright © 2018 <a href="#">DHL</a></strong> All rights
					reserved.
				</footer>
				<!--footer end-->
			</section>

			<!-- /.control-sidebar -->
			<!-- <?php include('include/control_sidebar.php'); ?> -->

	</div>	
    
	
	<!-- Bootstrap 3.3.6 -->
	<script src="<?= base_url() ?>public/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url() ?>public/dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="<?= base_url() ?>public/dist/js/demo.js"></script>
	<!-- page script -->
	<script type="text/javascript">
	  $(".flash-msg").fadeTo(2000, 500).slideUp(500, function(){
	    $(".flash-msg").slideUp(500);
	});
	</script>
	
	</body>
</html>
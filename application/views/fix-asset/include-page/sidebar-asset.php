<?php 
$cur_tab = $this->uri->segment(2)==''?'dashboard': $this->uri->segment(2);  
?>  

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less --> 
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url() ?>public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <br>
          <p><?= ucwords($this->session->userdata('name')); ?></p>
          <!-- <a href="#"><i class="fa fa-circle text-success"></i> <?= ucwords($this->session->userdata('whse')); ?></a> -->
        </div>
      </div>
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      
     

      <ul class="sidebar-menu">
        <li class="header">Overview</li>
          <li><a href="<?= base_url('admin/dashboard/dashboard_admin'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Dashboard</span></a></li>
          <!-- <li><a href="<?= base_url('admin/servicecenter/service_center_list'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Change Service Center</span></a></li> -->
          <!-- <li><a href="<?= base_url('fix-asset/item_asset/assigned_owner'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Fix Asset List</span></a></li> -->
          <!-- <li><a href="<?= base_url('fix-asset/item_asset/unassign_owner'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Unassign Asset Owner</span></a></li> -->
          <li><a href="<?= base_url('fix-asset/fix_asset/fix_asset_management'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Fix Asset List</span></a></li>
          <li><a href="<?= base_url('fix-asset/item_asset/asset_expired'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Asset Expired</span></a></li>
          <!-- <li><a href="<?= base_url('admin/items/item_transaction_all'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Print QR Code</span></a></li> -->
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Expire in  0-30 days</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Expire in 31-60 days</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Expire in 61-90 days</span></a></li>
      
        <li class="header">Requestor</li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>New Request</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Review</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Dispoal</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Approved</span></a></li>

       <li class="header">Approver</li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Wait to approve</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Dispoal</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Approved</span></a></li>
          


   <li class="header">Master Data</li>
        <li id="items" class="treeview">
            <a href="#">
            <i class="fa fa-circle-o text-aqua"></i> <span>Fix Assets Management</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="view_users" class=""><a href="<?= base_url('fix-asset/fix_asset/fix_asset_management'); ?>"><i class="fa fa-circle-o"></i> Item Assets Master</a></li>
			        <!-- <li id="user_group" class=""><a href="<?= base_url('fix-asset/asset_category/asset_category_management'); ?>"><i class="fa fa-circle-o text-danger"></i> Assets Category</a></li> -->
              <li id="user_group" class=""><a href="<?= base_url('fix-asset/asset_type/asset_type_management'); ?>"><i class="fa fa-circle-o"></i> Assets Type</a></li>
              <li id="user_group" class=""><a href="<?= base_url('fix-asset/fix_asset/usage_management'); ?>"><i class="fa fa-circle-o"></i> Usage Status</a></li>
            </ul>
        </li>

        <!-- <li id="items" class="treeview">
            <a href="#">
            <i class="fa fa-circle-o text-danger"></i> <span>Asset Owner</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="view_users" class=""><a href="<?= base_url('fix-asset/asset_owner/asset_owner_management'); ?>"><i class="fa fa-circle-o text-danger"></i> Asset Owner List</a></li>
			        <li id="user_group" class=""><a href="<?= base_url('fix-asset/department/department_management'); ?>"><i class="fa fa-circle-o text-danger"></i> Department</a></li>
            </ul>
        </li> -->


        <li><a href="<?= base_url('fix-asset/location/location_management'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Location</span></a></li>
        <li><a href="<?= base_url('fix-asset/dept/dep_management'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Dep Key</span></a></li>
        <li><a href="<?= base_url('fix-asset/asset_class/asset_class_management'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Asset Class</span></a></li>
        <li><a href="<?= base_url('fix-asset/import_data/excel_import'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Import Excel Data</span></a></li>
        <!-- <li><a href="<?= base_url('fix-asset/vendor/vendor_management'); ?>"><i class="fa fa-circle-o text-danger"></i> <span>Vendor List</span></a></li> -->
        <!-- <li><a href="<?= base_url('fix-asset/location/location_management'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Level Approve</span></a></li> -->
        <li class="header">USER</li>
        <!-- <li id="users" class="treeview">
            <a href="#">
            <i class="fa fa-circle-o text-aqua"></i> <span>Users Management</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="add_user"><a href="<?= base_url('admin/users/add'); ?>"><i class="fa fa-circle-o"></i> Add User</a></li>
              <li id="view_users" class=""><a href="<?= base_url('admin/users'); ?>"><i class="fa fa-circle-o"></i> View Users</a></li>
            </ul>
        </li> -->
 
        <li><a href="<?= site_url('admin/auth/logout'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Log out</span></a></li>
      </ul>


    </section>
    <!-- /.sidebar -->
  </aside>

  
<script>
  $("#<?= $cur_tab; ?>").addClass('active');
</script>

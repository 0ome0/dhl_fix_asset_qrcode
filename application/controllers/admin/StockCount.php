<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class StockCount extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function _example_output($output = null)
	{
		//$this->load->view('admin/layout-transfer-out.php',(array)$output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function generate_item_list()
	{
		$whse = $this->session->userdata('whse');
		$username = $this->session->userdata('name');

		//1. select data from v_item_whse insert to t_stockcount

		$sql = "INSERT INTO t_stockcount (id_item, id_center, item_description, item_code, whse_center_code ,qty_on_hand,shelf_location
		,qty_count,count_status, username)

		-- query protect duplicate item 
		SELECT t1.item_id, t1.id_center, t1.item_description, t1.item_code, t1.whse_center_code, t1.qty_on_hand, t1.shelf_location,'','1','$username'
		FROM v_item_whse t1
		left outer join t_stockcount as t2 on t1.item_id = t2.id_item and t1.id_center = t2.id_center and t1.item_code = t2.item_code
		WHERE t1.whse_center_code = '$whse' and t2.id_item is null
		;";

		$query = $this->db->query($sql);

		redirect(base_url('admin/StockCount/count_list'));  

        // echo("generate_item_list");
    }

    public function count_list()
	{
		$whse = $this->session->userdata('whse');
	
			$crud = new grocery_CRUD();
		
			$crud->set_theme('flexigrid');
			$crud->where('whse_center_code',$whse);
			$crud->set_table('t_stockcount');
			$crud->order_by('item_code', 'ASC');
			$crud->set_relation('count_status','t_stockcount_status','count_status',null,'status_id ASC');
			

			$crud->columns('count_date','whse_center_code','item_code','item_description','shelf_location','qty_count','count_status','username');

			// Readonly Field
			
			$crud->field_type('item_code','readonly');
			$crud->field_type('item_description','readonly');
			$crud->field_type('whse_center_code','readonly');
			$crud->field_type('qty_on_hand','readonly');
			$crud->field_type('shelf_location','readonly');
			$crud->field_type('count_date','readonly');
			$crud->field_type('count_status','readonly');
			$crud->field_type('username','readonly');

			// Hidden Field
			$crud->field_type('id_item','hidden');
			$crud->field_type('id_center','hidden');
			$crud->field_type('qty_on_hand','hidden');
			
			$crud->unset_add();
			// $crud->order_by('trans_date','desc');
			// callback change status from pending to counted 
			$crud->callback_after_update(array($this, 'counted'));

			
			// render view
			$crud->set_subject('Item Count List');
			$output = $crud->render();
			
			// $this->_example_output($output);
			$this->load->view('admin/layout-stockcount-list.php',(array)$output);
        // echo("count_list");
    }

	// function for update status after update qty count ( change status from pendng to counted )
	public function counted($post_array,$primary_key){
				  
			$sql_update_count_status = "UPDATE t_stockcount 
			SET count_status = '2' 
			where id = '$primary_key' ;
			";
			$query_stock = $this->db->query($sql_update_count_status);

		return $post_array;
	}

	public function counted_list(){
		// 
		
		$whse = $this->session->userdata('whse');
	
			$crud = new grocery_CRUD();
		
			$crud->set_theme('flexigrid');
			$crud->where('whse_center_code',$whse);
			$crud->where('t_stockcount.count_status','2');
			$crud->set_table('t_stockcount');
			$crud->order_by('item_code', 'ASC');
			$crud->set_relation('count_status','t_stockcount_status','count_status',null,'status_id ASC');
			

			$crud->columns('count_date','whse_center_code','item_code','item_description','shelf_location','qty_count','count_status','username');

			// Readonly Field
			
			$crud->field_type('item_code','readonly');
			$crud->field_type('item_description','readonly');
			$crud->field_type('whse_center_code','readonly');
			$crud->field_type('qty_on_hand','readonly');
			$crud->field_type('shelf_location','readonly');
			$crud->field_type('count_date','readonly');
			$crud->field_type('count_status','readonly');
			$crud->field_type('username','readonly');

			// Hidden Field
			$crud->field_type('id_item','hidden');
			$crud->field_type('id_center','hidden');
			
			$crud->unset_operations();

			$crud->set_subject('Process Counted List');
			$output = $crud->render();
			
			// $this->_example_output($output);
			$this->load->view('admin/layout-stockcounted-list.php',(array)$output);
	}

	public function process_counted(){
		//echo("please process adjust button");
		$whse = $this->session->userdata('whse');


		$sql = "INSERT INTO t_transaction (item_id, transaction_type, type_reason, qty,note,ref_document,trans_date,center_code,username, transfer_id_ref)
		SELECT item_id, transaction_type, type_reason, qty,note,ref_document,trans_date,center_code,username, transfer_id_ref
		FROM v_stockcount
		WHERE center_code = '$whse';
		";
		$query = $this->db->query($sql);

			
			  

			  // 1. Update item qty onhand @ t_item_whse.qty_on_hand
				$sql_update_stock = "UPDATE t_item_whse as t1
				JOIN v_item_on_hand as t2 ON t1.item_code = t2.item_id and t1.id_center = t2.id_center
				SET t1.qty_on_hand = t2.qty_balance ;
				";
				$query_stock = $this->db->query($sql_update_stock);

				// 2. clear temp stock count list ( all status pending and counted )
				$sql = "delete from  t_stockcount 
				WHERE whse_center_code = '$whse';
				;";
				$query = $this->db->query($sql);


		redirect(base_url('admin/Items/item_stock_onhand'));  
	}


    // public function confirm_counted()
	// {
	// 	//  echo("confirm_counted");
    // }
}
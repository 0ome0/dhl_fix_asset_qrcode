<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Receive extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function _example_output($output = null)
	{
		$this->load->view('admin/layout-receive.php',(array)$output);
	}

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function receive_transaction()
	{

		$whse = $this->session->userdata('whse');
		
			$crud = new grocery_CRUD();
		
			$crud->set_theme('flexigrid');
			$crud->where('t_transaction.center_code',$whse);
			$crud->set_table('t_transaction');


			
			// required field
			$crud->required_fields('item_id','type_reason','qty');

			$crud->set_relation('item_id','v_item_whse','{item_code} - {item_description}',array('whse_center_code' => $whse ));
			$crud->set_primary_key('item_id','v_item_whse');
			$crud->where('whse_center_code',$whse);			

			
			$crud->set_relation('type_reason','t_receive_reason','receive_reason',null,'id ASC');
			$crud->display_as('type_reason','Reason');
			$crud->display_as('item_id','item code');
			$crud->display_as('center_code','Service Center');
			
			$crud->order_by('id','desc');
			$crud->columns('trans_date','item_id','qty','type_reason','username','center_code');

			$crud->where('transaction_type','r');
			

			// unset column
			$crud->unset_add_fields(array('trans_date'));
			$crud->unset_columns(array('transaction_type'));

			// callback function
			// set trans date = now
           

			// set transaction_type = i (issue)
            $crud->callback_add_field('transaction_type', function () {
                return ' <input type="hidden" maxlength="5" value="Receive Transaction" name="transaction_type" > ';
			});
			
			// $crud->callback_add_field('trans_date', function () {
			// 	// $trans_date = date("d-m-y H:i:s");
			// 	date_default_timezone_set('Asia/Bangkok');
			// 	$trans_date = date("d/m/y H:i:s");

            //     return ' <input type="text" value='.$trans_date.' name="trans_date" > '.$trans_date.'';
            // });

			// set qty to negative before insert
			$crud->callback_before_insert(array($this,'set_transaction'));
			$crud->callback_after_insert(array($this,'update_qty_onhand'));

			//set service center
			$crud->callback_add_field('center_code', function () {
				// return ' <input id="field-center_code" class="form-control" name="center_code" value="'.ucwords($this->session->userdata('whse')).'" maxlength="50" type="text" readonly> ';
				return ' <input type="hidden" name="center_code" value="'.ucwords($this->session->userdata('whse')).'" maxlength="50" type="text"> ';
			});

			//set username
			$crud->callback_add_field('username', function () {
                return ' <input type="hidden" name="username" value="'.ucwords($this->session->userdata('name')).'" maxlength="50"> ';
			});

			
			$crud->field_type('transaction_type','hidden');
			$crud->field_type('center_code','hidden');
			$crud->field_type('username','hidden');
			$crud->field_type('transfer_id_ref','hidden');
			
			$crud->unset_delete();
			$crud->unset_edit();
			
			// render view
			$crud->set_subject('Issue Transaction');
			$output = $crud->render();

			$this->_example_output($output);
	}
	
	public function receive_reason_manangement()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_issue_reason');
			// $crud->set_relation('item_status','t_status','status');
            // $crud->display_as('item_status','Status');

            // $crud->set_relation('item_um_code','t_unit','unit');
            // $crud->display_as('item_um_code','Stock Unit');
            
			$crud->set_subject('Receive Reason Master');

			// $crud->required_fields('lastName');

			// $crud->set_field_upload('center_map','assets/uploads/files');

			$output = $crud->render();


			// $this->_example_output($output);
			$this->load->view('admin/layout-receive-reason.php',(array)$output);
	}

	function set_transaction($post_array) {
		
		// if(!empty($post_array['qty']))
		// {
		// 		$qty = $post_array['qty'];
		// 		$neg_flag = '-1';
		// 		$neg_qty = $qty*$neg_flag;
		// 		$post_array['qty'] = $neg_qty;
				
		// }
		// else
		// {
		// unset($post_array['qty']);
		// }

		$post_array['transaction_type'] = 'r';
		$transaction_type = $post_array['transaction_type'];
		
		return $post_array;
		}
	

		function update_qty_onhand($post_array) {
			
					$item_code = $post_array['item_id'];
					// $sql = "update t_item_whse set qty_on_hand = 99 where item_code = $item_code ";
					$sql = "UPDATE t_item_whse as t1
					JOIN v_item_on_hand as t2 ON t1.item_code = t2.item_id and t1.id_center = t2.id_center
					SET t1.qty_on_hand = t2.qty_balance ;
					";
					$query = $this->db->query($sql);
					return $post_array;
			
		}
			
			
}
			
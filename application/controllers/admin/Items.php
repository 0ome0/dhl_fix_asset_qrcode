<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Items extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function _example_output($output = null)
	{
		$this->load->view('admin/layout-items.php',(array)$output);
	}

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function item_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_item');
			$crud->set_relation('item_status','t_status','status');
            $crud->display_as('item_status','Status');

            $crud->set_relation('item_um_code','t_unit','unit');
			$crud->display_as('item_um_code','Stock Unit');
			
			$crud->set_relation('item_category_id','t_item_category','category_name');
			$crud->display_as('item_category_id','Category Name');
			
			$crud->set_relation('item_type_id','t_item_type','type_name');
            $crud->display_as('item_type_id','Category Name');
            
			$crud->set_subject('Item Master');

			// set upload image field
			$crud->set_field_upload('item_image','assets/uploads/files');

			// set item + warehouse
			$crud->set_relation_n_n('Service_Center', 't_item_whse', 't_service_center', 'item_code', 'id_center', 'center_code'  );

			// set item + unit
			$crud->set_relation_n_n('Unit_Convert', 't_item_unit', 't_unit', 'item_code', 'id_unit', 'unit'  );
			

			$crud->callback_before_insert(array($this,'set_barcode_id'));

			$output = $crud->render();

			$this->_example_output($output);
	}
	
	public function item_category_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_item_category');
			// $crud->set_relation('item_status','t_status','status');
            // $crud->display_as('item_status','Status');

            // $crud->set_relation('item_um_code','t_unit','unit');
            // $crud->display_as('item_um_code','Stock Unit');
            
			$crud->set_subject('Item Category Master');

			// $crud->required_fields('lastName');

			// $crud->set_field_upload('center_map','assets/uploads/files');

			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('admin/layout-item-category.php',(array)$output);
	}

	public function item_stock_onhand()
	{
			$whse = $this->session->userdata('whse');

			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');

			
			$crud->set_table('t_item_whse');

			$crud->where('center_code',$whse);
			$crud->set_relation('id_center','t_service_center','center_code');
         
			$crud->set_relation('item_code','t_item','{item_code} - {item_description}');
			
			$crud->columns('id_center','item_code','qty_on_hand','qty_minimum','lead_time','shelf_location');			

			$crud->display_as('qty_balance','Qty balance');
			$crud->display_as('center_code','Service Center');
			

			// $crud->callback_column('qty_on_hand',array($this,'_callback_check_minimum_qty'));
			
			
			// $crud->unset_operations();
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			$crud->set_subject('Item Stock Onhand');


			$output = $crud->render();

			$this->load->view('admin/layout-item-stock-onhand.php',(array)$output);
	}

	public function item_transaction()
	{	
			$whse = $this->session->userdata('whse');
			
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->where('v_item_transaction.center_code',$whse);
			$crud->set_table('v_item_transaction');
			$crud->set_primary_key('id');

			$crud->set_relation('transaction_type','t_transtype_code','type_description');
					
			$crud->columns('trans_date','type_description','reason','item_code','item_description','qty','unit','ref_document','center_code','username');

			$crud->order_by('trans_date','desc');

			// set hidden field when click show transaction detail 
			$crud->field_type('id','hidden');
			$crud->field_type('type_reason','hidden');
			$crud->field_type('transaction_type','hidden');
			$crud->field_type('item_id','hidden');
			$crud->field_type('item_um_code','hidden');
			$crud->field_type('item_type_id','hidden');

			$crud->display_as('center_code','Service Center');
			$crud->display_as('ref_document','Ref Doc.');
			
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();

			$crud->set_subject('Item By Transaction');
			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('admin/layout-item-transaction.php',(array)$output);
	}

	public function item_transaction_all()
	{	
			// $whse = $this->session->userdata('whse');
			
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			// $crud->where('v_item_transaction.center_code',$whse);
			$crud->set_table('v_item_transaction');
			$crud->set_primary_key('id');

			$crud->set_relation('transaction_type','t_transtype_code','type_description');
					
			$crud->columns('trans_date','type_description','reason','item_code','item_description','qty','unit','ref_document','center_code','username');

			$crud->order_by('trans_date','desc');

			// set hidden field when click show transaction detail 
			$crud->field_type('id','hidden');
			$crud->field_type('type_reason','hidden');
			$crud->field_type('transaction_type','hidden');
			$crud->field_type('item_id','hidden');
			$crud->field_type('item_um_code','hidden');
			$crud->field_type('item_type_id','hidden');

			$crud->display_as('center_code','Service Center');
			$crud->display_as('ref_document','Ref Doc.');
			
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();

			$crud->set_subject('Item By Transaction');
			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('admin/layout-item-transaction.php',(array)$output);
	}

	public function item_stock_lower()
	{
			$whse = $this->session->userdata('whse');

			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');

			
			$crud->set_table('v_item_whse');
			$crud->set_primary_key('id');

			$crud->where('center_code',$whse);
			$crud->where('qty_on_hand <= qty_minimum');
			
			$crud->set_relation('id_center','t_service_center','center_code');
		
			$crud->set_relation('item_id','t_item','{item_code} - {item_description}');
			
			$crud->columns('id_center','item_code','item_description','qty_on_hand','qty_minimum','lead_time','shelf_location');			

			$crud->display_as('qty_balance','Qty balance');
			$crud->display_as('center_code','Service Center');
		

		// $crud->callback_column('qty_on_hand',array($this,'_callback_check_minimum_qty'));
		
		
		// $crud->unset_operations();

            $crud->unset_operations();
			$crud->set_subject('Item Stock Lower Stock Alert');


			$output = $crud->render();

			$this->load->view('admin/layout-item-stock-lower.php',(array)$output);
	}


	public function item_stock_lower_all()
	{
			$whse = $this->session->userdata('whse');

			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');

			
			$crud->set_table('v_item_whse');
			$crud->set_primary_key('id');

			// $crud->where('center_code',$whse);
			$crud->where('qty_on_hand <= qty_minimum');
			
			$crud->set_relation('id_center','t_service_center','center_code');
		
			$crud->set_relation('item_id','t_item','{item_code} - {item_description}');
			
			$crud->columns('id_center','item_code','item_description','qty_on_hand','qty_minimum','lead_time','shelf_location');			

			$crud->display_as('qty_balance','Qty balance');
			$crud->display_as('center_code','Service Center');
		

		// $crud->callback_column('qty_on_hand',array($this,'_callback_check_minimum_qty'));
		
		
		// $crud->unset_operations();

            $crud->unset_operations();
			$crud->set_subject('Item Stock Lower Stock Alert');


			$output = $crud->render();

			$this->load->view('admin/layout-item-stock-lower.php',(array)$output);
	}


	public function item_whse_management (){

		
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_item_whse');
			// $crud->set_primary_key('item_code','id_center');
			$crud->set_relation('item_code','t_item','item_code');
			$crud->set_relation('id_center','t_service_center','center_code');

			$crud->unset_add();
			$crud->unset_delete();



			$crud->display_as('id_center','Service Center');
			$crud->display_as( 'lead_time_day','Lead time');
			

			$crud->set_subject('Item and Service Center Mapping');
			$crud->field_type('item_code','readonly');
			$crud->field_type('id_center','readonly');
			$crud->field_type('qty_on_hand','readonly');
			

			$output = $crud->render();
			$this->load->view('admin/layout-item-whse.php',(array)$output);
			

	}

	public function set_barcode_id($post_array){
		
		if(!empty($post_array['item_barcode']))
		{
				// $barcode_id = strtoupper(uniqid());
				$barcode_id = '999999';
				$post_array['item_barcode'] = $barcode_id ;				
		}
		else
		{
		unset($post_array['item_barcode']);
		}
		
		return $post_array;
	}

	// public function _callback_check_minimum_qty($row){
	// 	if ($row->qty_on_hand  <= 0 ){
	// 		return "<pre style='color:red'>".$row->qty_on_hand."</pre>";}
	// 		else {return $row->qty_on_hand;}
	// }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Change_asset extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

        $this->load->library('grocery_CRUD');
        $this->load->model('admin/auth_model', 'auth_model');

        // $this->load->model('itasset/Vendor_model', 'Vendor_model');
        $this->load->model('itasset/Item_asset_model', 'Item_asset_model');
	}

	public function load_login()
	{
        
        $qr_code_id = $this->input->post('qr_code_id');
        $data['qr_code_id'] =  $qr_code_id;
        // echo $qr_code_id;
        $this->load->view('itasset/layout-login_change',$data);

    }
    

    public function check_login()
	{
        // Function to back previous page
        // if ( ! function_exists('redirect_back'))
        // {
        // function redirect_back()
        //     {
        //         if(isset($_SERVER['HTTP_REFERER']))
        //         {
        //             header('Location: '.$_SERVER['HTTP_REFERER']);
        //         }
        //         else
        //         {
        //             header('Location: http://'.$_SERVER['SERVER_NAME']);
        //         }
        //         exit;
        //     }
        // } 

        $qr_code_id = $this->input->post('qr_code_id');
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        // echo $qr_code_id ;
        // echo $username ;
        // echo $password ;

        // 1. check login
            $data = array(
                // 'email' => $this->input->post('email'),
                'username' => trim($username),
                'password' => trim($password)
            );

                $result = $this->auth_model->login($data);
					if ($result == TRUE) {
						$admin_data = array(
							'admin_id' => $result['id'],
							 'name' => $result['username'],
							 // add warehouse and user profile set session
							 'whse' => $result['center_code'],
							 'email' => $result['email'],
							 'firstname' => $result['firstname'],
							 'lastname' => $result['lastname'],
							 'role' => $result['role'],
						 	'is_admin_login' => TRUE
						);
						$this->session->set_userdata($admin_data);
						// redirect(base_url('admin/dashboard'), 'refresh');
						
                        // redirect(base_url('admin/dashboard/dashboard_admin'), 'refresh');
                        echo "<h1 style='text-align:center; padding : 3%;' >Welcome $username</h1>";
                        // echo " - ".$qr_code_id ;

                            $result = $this->Item_asset_model->get_item_asset_detail($qr_code_id);
        
                            if ($result == TRUE) {
                                // echo json_encode($result);
                    
                                // $this->load->model('itasset/Item_asset_model', 'Item_asset_model');
                                // $data['item_detail'] =  $this->Item_asset_model->get_item_by_id($id);

                                $data['item_detail'] = $this->Item_asset_model->get_item_asset_detail($qr_code_id);   
                                $data['location'] =  $this->Item_asset_model->get_location();
                                $data['asset_owner'] =  $this->Item_asset_model->get_asset_owner();
                                $data['usage_status'] =  $this->Item_asset_model->get_usage_status();
                    
                                // $this->load->view("itasset/layout-qr_result.php", $data);
                                $this->load->view("itasset/layout-qr_result_edit.php", $data);

                                
                                    // 2. load model data
                                    //     2.1 location
                                    //     2.2 asset owner
                                    //     2.3 asset usage status
                                    //     2.4 v_asset_item     
                                    // 3. send data to view result to change 

                                    // $data['qr_code_id'] =  $qr_code_id;
                                    // // echo $qr_code_id;
                                    // $this->load->view('itasset/layout-login_change',$data);
                    
                            }
                            else{
                                $data['qr_code_id'] = $qr_code_id;
                                $data['msg'] = "Invalid Email or Password!";
                                $this->load->view('itasset/layout-login_change_failed', $data);
                            }
                


					}
					else{
                        
                        $data['qr_code_id'] = $qr_code_id;
                        $data['msg'] = "Invalid Email or Password!";
                        $this->load->view('itasset/layout-login_change_failed', $data);
              		}       

    }

    public function submit_change(){

        $qr_code_id = $this->input->post('qr_code_id');
        $new_location = $this->input->post('new_location');
        $new_asset_owner = $this->input->post('new_asset_owner');
        $new_usage_status = $this->input->post('new_usage_status');

        $data = array(
            // 'email' => $this->input->post('email'),
            'location' => $new_location,
            'asset_owner' => $new_asset_owner,
            'use_status' => $new_usage_status
            // 'qr_code_id' => $qr_code_id
        );

         
                        // insert log table before edit. 2018-06-16
                        $username = $this->session->userdata('name');
                        $this->load->model('itasset/Item_asset_model', 'Item_asset_model');
			            $this->Item_asset_model->insert_item_asset_log_by_qrcode($qr_code_id,$username);

                        // end of insert log

                        $result = $this->Item_asset_model->edit_item_asset($data, $qr_code_id);
                        if($result){
                        $data['msg'] = "Asset Updated Successful!";
                        // $this->load->view('itasset/layout-login_change_failed', $data);
                        // echo $data['msg'];
                            date_default_timezone_set("Asia/Bangkok");
                            $last_updated = date("Y-m-d H:i:s");

                            $this->load->model('itasset/Item_asset_model', 'Item_asset_model');
                            $this->Item_asset_model->updated_item_asset_record_by_qrcode($qr_code_id,$username,$last_updated);
                       

                        echo "<script>alert('Asset Updated Successful!');</script>";
                        redirect(base_url('api/qr_scan/scan?qr_code_id='.$qr_code_id.''), 'refresh');

                    }
                    
                    echo "Can not update date !";

        // echo $qr_code_id ;
        // echo " - ";
        // echo $new_location ;
        // echo " - ";
        // echo $new_asset_owner ;
        // echo " - ";
        // echo $new_usage_status ;

    }


	

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asset_owner extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		// $this->load->model('itasset/Vendor_model', 'Vendor_model');
	}

	public function _example_output($output = null)
	{
		$this->load->view('itasset/layout-asset_owner.php',(array)$output);
	}

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function asset_owner_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_asset_owner');
			$crud->set_relation('asset_owner_status','t_status','status',null,'status desc');
            $crud->display_as('status','status');
            
            $crud->set_relation('department','t_department','department_name',null,'department_name asc' );
            $crud->display_as('department','Department');
            
			$crud->set_subject('Asset Owner');

			$crud->required_fields('first_name');
            $crud->required_fields('last_name');
            $crud->required_fields('asset_owner_status');

			// $crud->set_field_upload('location_image','assets/uploads/files');
			// // $crud->field_type('center_code','readonly');

			$output = $crud->render();

			$this->_example_output($output);
	}

}
